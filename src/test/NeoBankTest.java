package test;

import fragment.NeoBankLoginFragment;
import page.NeoBankLoginPage;

import static utils.SoftAssertUtils.getSoftAssertions;

public class NeoBankTest {
    private final NeoBankLoginPage neoBankLoginPage = new NeoBankLoginPage();
    private final NeoBankLoginFragment neoBankLoginFragment = new NeoBankLoginFragment();

    private static final String phone = "0950763692";
    private static final String message = "Відкриття бізнес-рахунку можливе тільки через додаток NEOBANK для бізнесу";

    public void checkLoginPage() {
        neoBankLoginPage.visit(ENDPOINTS_CONFIG.loginPath());
        neoBankLoginFragment.waitForShown();

        getSoftAssertions().assertThat(neoBankLoginPage.isCurrentByTitle())
                as.(description: "Page title is wrong")
                .isTrue();

        getSoftAssertions().assertThatCode(() -> neoBankLoginFragment.enterPhoneNumber(phone))
                as.(description: "Enter phone number")
                .doesNotThrowAnyException();

        getSoftAssertions().assertThatCode(neoBankLoginFragment::clickContinueButton)
                as.(description: "Click continue button")
                .doesNotThrowAnyException();

        getSoftAssertions().assertThatCode(neoBankLoginFragment::checkNotificationTextIsVisible)
                as.(description: "Notification text is visible")
                .doesNotThrowAnyException();

        getSoftAssertions().assertThatCode(() -> neoBankLoginFragment.checkTextFromNotification(message))
                as.(description: "Check text from notification")
                .doesNotThrowAnyException();

        public static void closeWindow() {
            SelenideLogger.run (source: "current window", SelenideLogger.getReadableSubject(methodName: "close", new Object[0]), WebDriverRunner::closeWindow);
        }
    }
}