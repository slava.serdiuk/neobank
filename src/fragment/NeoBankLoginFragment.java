package fragment;

public class NeoBankLoginFragment {
    private final By rootElement = By.cssSelector(".auth-form");
    private final By mobileNumberField = By.xpath(".//input[@id='login']");
    private final By buttonContinue = By.cssSelector(".col-12");
    private final By textNotification = By.cssSelector(".auth-form-title");

    public NeoBankLoginFragment() {
        setRootElement(rootElement);
    }


    @Step("Enter phone number")
    public void enterPhoneNumber(final String phone) {
        getChildElement(mobileNumberField).setValue(phone);
    }

    @Step("Click continue button")
    public void clickContinueButton() {
        getChildElement(buttonContinue).click();
    }

    @Step("Notification text is visible")
    public void checkNotificationTextIsVisible() {
        getChildElement(textNotification).shouldBe(visible);
    }

    @Step("Check text from notification")
    public void checkTextFromNotification(final String message) {
        getChildElement(textNotification).shouldHave(exactText(message));
    }
}