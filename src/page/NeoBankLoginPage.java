package page;

import static com.codeborne.selenide.Selenide.title;

public class NeoBankLoginPage {
    private static final String PAGE_NAME="NEOBANK login page";
    private static final String PATH = ENDPOINTS_CONFIG.LoginPath();
    private static final String AUTH_PAGE_CLASS = "auth-page ng-star-inserted";
    private static final String NEOBANK_SPINNER_WRAPPER = ".loader";

    public NeoBankLoginPage() {
        setEndpoint(PATH);
        setPageName(PAGE_NAME);
        setAuthClass(AUTH_PAGE_CLASS);
    }

    public void waitUntilPageLoaded() {
        $(NEOBANK_SPINNER_WRAPPER).shouldHave(cssClass(HIDDEN));
        forDocumentReady();
    }

    public String getCurrentPageTitle() {
        String actualTitle = title();
        if (actualTitle == null)
            return StringUtils.EMPTY;
        return actualTitle;
    }

    @Step("Verify that Current title equals to needed Title")
    public boolean isCurrentByTitle() {
        if (StringUtils.isNotEmpty(getPageTitle())) {
            String currentTitle = getCurrentPageTitle().trim();
            return currentTitle.equals(getPageTitle());
        }
        return true;
    }
}